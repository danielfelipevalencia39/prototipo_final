
package connector;

/**
 *
 * @author FELIPE
 */
public class ConexionObjetos {
    private BeanConnector bConn;
    private static ConexionObjetos cxObjects;
    public ConexionObjetos(){
        try{
            bConn = new BeanConnector();
            bConn.setDriver("com.mysql.jdbc.Driver");
            bConn.setUser("root");
            bConn.setPassword("root");
            bConn.setUrl("jdbc:mysql://localhost:3306/instrumentos?serverTimezone=UTC");
            bConn.conectar();
        }catch(Exception e){
            e.printStackTrace();
        }  
    }
    public static ConexionObjetos getInstance(){
        if(cxObjects == null){
            cxObjects = new ConexionObjetos();
        }else{
            System.out.println("ERROR, ESYA TRATANDO DE INSTANCIAR UN OBJETO SINGLETON QUE YA EXISTE");
        }
        return cxObjects;
    }
@Override
public ConexionObjetos clone(){
    try{
        throw new CloneNotSupportedException();
    }catch(CloneNotSupportedException e){
        System.out.println("ESTE OBJETO NO SE PUEDE CLONAR, SOY UNICO - SINGLETON ");
    }
    return null;
}
 public BeanConnector getbConn(){
            return bConn;
      }
}
