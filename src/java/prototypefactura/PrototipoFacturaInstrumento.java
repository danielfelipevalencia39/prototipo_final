
package prototypefactura;

/**
 *
 * @author FELIPE
 */
public abstract class PrototipoFacturaInstrumento implements Cloneable {
    private String marca;
    private int referencia;
    private int codigoFactura;
    
    public PrototipoFacturaInstrumento(String marca, int referencia ,int codigoFactura ){
        this.marca = marca;
        this.codigoFactura = codigoFactura;
        this.referencia = referencia;
    }
    public Object clone() throws CloneNotSupportedException{
        return super.clone();   
    }
    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }
    public int getReferencia() {
        return referencia;
    }

    public void setReferencia(int referencia) {
        this.referencia = referencia;
    }
    public int getCodigoFactura() {
        return codigoFactura;
    }

    public void setCodigoFactura(int codigoFactura) {
        this.codigoFactura = codigoFactura;
    }


    
    
}
