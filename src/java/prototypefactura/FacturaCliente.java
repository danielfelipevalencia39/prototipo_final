
package prototypefactura;



/**
 *
 * @author FELIPE
 */
public class FacturaCliente {
    public static void main (String args [])throws CloneNotSupportedException{
                int capacidadLoteInstrumentos = 10;
        PrototipoFacturaInstrumento prototipoVentaDetal = new FacturaVentaDetal("Eastwood & Airlines", 321 , 0, 7000000);
        PrototipoFacturaInstrumento prototipoVentaporMenor = new FacturaVentaPorMenor("Yamaha", 654, 0, 1990000);
        PrototipoFacturaInstrumento prototipoVentaSinIVA = new FacturaVentaSinIVA("Ozeki", 987, 0, 150000);
        for(int i = 0; i < capacidadLoteInstrumentos; i++){
            PrototipoFacturaInstrumento ventaDetal = (PrototipoFacturaInstrumento) prototipoVentaDetal.clone();
            ventaDetal.setCodigoFactura(i*3);
            System.out.println(ventaDetal.getMarca() + " / " + ventaDetal.getReferencia() + 
                    " / " + ventaDetal.getCodigoFactura());
            PrototipoFacturaInstrumento ventaporMenor = (PrototipoFacturaInstrumento) prototipoVentaporMenor.clone();
            ventaporMenor.setCodigoFactura(i*4);
            System.out.println(ventaporMenor.getMarca()+ " / " + ventaporMenor.getReferencia() +
                    " / " + ventaporMenor.getCodigoFactura());
            PrototipoFacturaInstrumento ventaSinIVA = (PrototipoFacturaInstrumento) prototipoVentaSinIVA.clone();
            ventaSinIVA.setCodigoFactura(i*5);
            System.out.println(ventaSinIVA.getMarca() + " / " + ventaSinIVA.getReferencia() +
                    " / " + ventaSinIVA.getCodigoFactura());
        }
        
        
        
    }
}
