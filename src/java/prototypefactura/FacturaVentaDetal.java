
package prototypefactura;

/**
 *
 * @author FELIPE
 */
public class FacturaVentaDetal extends PrototipoFacturaInstrumento {
    private int comprobantePago;

    public FacturaVentaDetal(String marca, int referencia, int codigoFactura, int comprobantePago) {
        super(marca, referencia, codigoFactura);
        this.comprobantePago = comprobantePago;
    }
    public int getComprobantePago() {
        return comprobantePago;
    }

    public void setComprobantePago(int comprobantePago) {
        this.comprobantePago = comprobantePago;
    }
}
