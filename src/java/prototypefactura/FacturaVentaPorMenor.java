
package prototypefactura;

/**
 *
 * @author FELIPE
 */
public class FacturaVentaPorMenor extends PrototipoFacturaInstrumento {
    private int importe;
    public FacturaVentaPorMenor(String marca, int referencia, int codigoFactura, int importe) {
        super(marca, referencia, codigoFactura);
        this.importe = importe;
    }
    public int getImporte() {
        return importe;
    }

    public void setImporte(int importe) {
        this.importe = importe;
    }
    
}
