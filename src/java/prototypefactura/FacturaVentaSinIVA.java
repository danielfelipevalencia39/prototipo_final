
package prototypefactura;

/**
 *
 * @author FELIPE
 */
public class FacturaVentaSinIVA extends PrototipoFacturaInstrumento {
    private int valorExentoIVA;
    public FacturaVentaSinIVA(String marca, int referencia, int codigoFactura, int valorExtentoIVA) {
        super(marca, referencia, codigoFactura);
        this.valorExentoIVA = valorExtentoIVA;
    }
    public int getValorExentoIVA() {
        return valorExentoIVA;
    }

    public void setValorExentoIVA(int valorExentoIVA) {
        this.valorExentoIVA = valorExentoIVA;
    }
    
}
