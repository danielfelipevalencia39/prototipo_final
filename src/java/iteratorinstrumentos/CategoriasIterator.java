
package iteratorinstrumentos;

import java.util.Iterator;
import java.util.LinkedList;
import pojos.Category;

/**
 *
 * @author FELIPE
 */
public class CategoriasIterator implements Iterator<Category>{
    private LinkedList<Category> listaCategorias;
    private int counter = 0;
    public CategoriasIterator(LinkedList<Category> listaCategorias){
        this.listaCategorias = listaCategorias;
    } 
    public Category next(){
        return listaCategorias.get(counter++);
    }
    public boolean hasNext(){
        if(counter<listaCategorias.size() && listaCategorias.get(counter) != null){
            return true;
        } else {
            return false;
        }
    }  
}
