
package patronfacadeinstrumentos;

/**
 *
 * @author FELIPE
 */
public class InstrumentosFacade {
    private int id;
    private String nameInstrumento;
    private String descriptionInstrumento;
    private int priceInstrumento;
    private String categoryInstrumento;
    
    public InstrumentosFacade(int id, String nameInstrumento, String descriptionInstrumento,
    int priceInstrumento, String categoryInstrumento){
        this.id = id;
        this.nameInstrumento = nameInstrumento;
        this.descriptionInstrumento = descriptionInstrumento;
        this.priceInstrumento = priceInstrumento;
        this.categoryInstrumento = categoryInstrumento;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNameInstrumento() {
        return nameInstrumento;
    }

    public void setNameInstrumento(String nameInstrumento) {
        this.nameInstrumento = nameInstrumento;
    }

    public String getDescriptionInstrumento() {
        return descriptionInstrumento;
    }

    public void setDescriptionInstrumento(String descriptionInstrumento) {
        this.descriptionInstrumento = descriptionInstrumento;
    }

    public int getPriceInstrumento() {
        return priceInstrumento;
    }

    public void setPriceInstrumento(int priceInstrumento) {
        this.priceInstrumento = priceInstrumento;
    }

    public String getCategoryInstrumento() {
        return categoryInstrumento;
    }

    public void setCategoryInstrumento(String categoryInstrumento) {
        this.categoryInstrumento = categoryInstrumento;
    }
    
}
