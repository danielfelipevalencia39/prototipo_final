
package patronfacadeinstrumentos;

import connector.ConexionObjetos;
import cruds.CRUDInstrumentos;
import java.sql.ResultSet;
import java.util.LinkedList;
import pojos.Category;
import pojos.Instrumento;

/**
 *
 * @author FELIPE
 */
public class FacadeInstrumentos {
    private CRUDInstrumentos crudInstrumentos;
    private ConexionObjetos conn;
    
    public FacadeInstrumentos(){
       crudInstrumentos = new CRUDInstrumentos();
       conn = new ConexionObjetos();
    }
    public LinkedList<InstrumentosFacade> generarInstrumentosFacade(){
        LinkedList<InstrumentosFacade> listaInstrumentosCategoria = new LinkedList<>();
        LinkedList<Instrumento> listaInstrumentos = crudInstrumentos.listarInstrumentos();
        LinkedList<Category> listaCategoriasInstrumento = new LinkedList<>();
        
        try{
            ResultSet rs = conn.getbConn().getSt().executeQuery("SELECT * FROM category");
            while (rs.next()){
            Category cat= new Category();
            cat.setId(rs.getInt("id"));
            cat.setName(rs.getString("name"));
            listaCategoriasInstrumento.add(cat);
        }
            rs.close();
        }catch (Exception e){
            e.printStackTrace();
        }
        for(int x=0;x<listaInstrumentos.size();x++){
            for(int y=0;y<listaCategoriasInstrumento.size();y++){
                if(listaInstrumentos.get(x).getCategory() == listaCategoriasInstrumento.get(y).getId()){
                   listaInstrumentos.get(x).setCategoryName(listaCategoriasInstrumento.get(y).getName());
                    System.out.println("Id Categoria >>> " + listaInstrumentos.get(x).getCategory());
                    System.out.println("Name Categoria >>> " + listaCategoriasInstrumento.get(y).getName());
                    System.out.println("////////////////////////////////////////////////////////////////////");
                }
            }
            Instrumento i = listaInstrumentos.get(x);
            InstrumentosFacade iF = new InstrumentosFacade(i.getId(), i.getName(),i.getDescription(), 
                    i.getPrice(), i.getCategoryName());
            listaInstrumentosCategoria.add(iF);
        }
        return listaInstrumentosCategoria;
    }
}
