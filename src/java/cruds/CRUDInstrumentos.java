
package cruds;

import builderinstrumentos.BuilderInstrumentoCuerdas;
import builderinstrumentos.BuilderInstrumentoElectronicos;
import builderinstrumentos.BuilderInstrumentoPercusion;
import builderinstrumentos.BuilderInstrumentoVientos;
import builderinstrumentos.InstrumentoDirector;
import connector.ConexionObjetos;
import java.sql.ResultSet;
import java.util.LinkedList;
import pojos.Instrumento;
/**
 *
 * @author FELIPE
 */
public class CRUDInstrumentos {
        private static ConexionObjetos conn;
    
    public CRUDInstrumentos(){
        try{
            conn= ConexionObjetos.getInstance();
           
        } catch (Exception e){
            e.printStackTrace();
        }
    }
    public static LinkedList<Instrumento> listarInstrumentos(){
        LinkedList<Instrumento> listaInstrumentos = new LinkedList<>();
        try{
            ResultSet rs = conn.getbConn().getSt().executeQuery("SELECT * FROM instrumento, category WHERE instrumento.category = category.id");
            while (rs.next()){
                Instrumento instrumento = new Instrumento();
                instrumento.setId(rs.getInt("id"));
                instrumento.setName(rs.getString("name"));
                instrumento.setDescription(rs.getString("description"));
                instrumento.setPrice(rs.getInt("price"));
                instrumento.setCategory(rs.getInt("category"));
                instrumento.setCategoryName(rs.getString("category.name"));
                
                listaInstrumentos.add(instrumento);
            }
            rs.close();
        }catch (Exception e){
            e.printStackTrace();
        }
        return listaInstrumentos;
    }
        public static LinkedList <Instrumento> listarInstrumentosBuilder(int categoryB) {
       LinkedList<Instrumento> listaInstrumentosCuerdas = new LinkedList<>();
       LinkedList<Instrumento> listaInstrumentosPercusion = new LinkedList<>();
       LinkedList<Instrumento> listaInstrumentosVientos = new LinkedList<>();
       LinkedList<Instrumento> listaInstrumentosElectronico = new LinkedList<>();
       InstrumentoDirector pd = new InstrumentoDirector();
       try{
           ResultSet rs = conn.getbConn().getSt().executeQuery("SELECT * FROM instrumento "+
                   " WHERE category = " + categoryB);
           while (rs.next()){
           if(categoryB == 1){
               pd.setInstrumentoBuilder(new BuilderInstrumentoCuerdas());
               pd.buildInstrumento(rs.getInt("id"),rs.getString("name"), rs.getString("description"), 
                       rs.getInt("price"), categoryB);
               listaInstrumentosCuerdas.add(pd.getInstrumentoPatronBuilder());
           } else if(categoryB == 2){
               pd.setInstrumentoBuilder(new BuilderInstrumentoPercusion());
               pd.buildInstrumento(rs.getInt("id"), rs.getString("name"), rs.getString("description"), 
                       rs.getInt("price"), categoryB);
               listaInstrumentosPercusion.add(pd.getInstrumentoPatronBuilder()); 
           }else if(categoryB == 3){
               pd.setInstrumentoBuilder(new BuilderInstrumentoVientos());
               pd.buildInstrumento(rs.getInt("id"), rs.getString("name"), rs.getString("description"), 
                       rs.getInt("price"), categoryB);
               listaInstrumentosVientos.add(pd.getInstrumentoPatronBuilder());
           }else if(categoryB == 4){
               pd.setInstrumentoBuilder(new BuilderInstrumentoElectronicos());
               pd.buildInstrumento(rs.getInt("id"), rs.getString("name"), rs.getString("description"), 
                       rs.getInt("price"), categoryB);
               listaInstrumentosElectronico.add(pd.getInstrumentoPatronBuilder());
          }
         }
           rs.close();
       }catch(Exception e){
           e.printStackTrace();
       }        
       if (categoryB == 1){
           return listaInstrumentosCuerdas;
       }else if(categoryB == 2){
           return listaInstrumentosPercusion;
       }else if (categoryB == 3){
           return listaInstrumentosVientos;
       } else if (categoryB == 4){
           return listaInstrumentosElectronico;
       }
       return null;
    }
       
    public void insertarInstrumento(Instrumento instrumento){
        String name = instrumento.getName();
        String description = instrumento.getDescription();
        int price = instrumento.getPrice();
        int category = instrumento.getCategory();
        try{
            conn.getbConn().getSt().executeUpdate("INSERT INTO instrumento (name, description, price, category) "+
                    "values ('"+ name + "', '" + description + "', '"+ price + "', '"+ category +"')" );
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    
    public void modificarInstrumento(Instrumento instrumento){
        int id = instrumento.getId();
        String name = instrumento.getName();
        String description = instrumento.getDescription();
        int price = instrumento.getPrice();
        int category = instrumento.getCategory();
    try{
        conn.getbConn().getSt().executeUpdate("UPDATE instrumento SET name='" + name + 
                "', description = '" + description +
                "', price = '" + price +
                "', category = '" + category +
                "' WHERE id = " +id);
    }catch (Exception e){
       e.printStackTrace();
    }
    
   }
    public void borrarInstrumento (int id){
        try{
            conn.getbConn().getSt().executeUpdate("DELETE FROM instrumento WHERE id = " + id);
        }catch(Exception e){
           e.printStackTrace();
        }
    }
}
