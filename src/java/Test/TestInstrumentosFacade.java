
package Test;


import connector.ConexionObjetos;
import cruds.CRUDInstrumentos;
import java.sql.ResultSet;
import java.util.LinkedList;
import patronfacadeinstrumentos.FacadeInstrumentos;
import patronfacadeinstrumentos.InstrumentosFacade;
import pojos.Instrumento;
/**
 *
 * @author FELIPE
 */
public class TestInstrumentosFacade {
    static FacadeInstrumentos facadeInstrumentos;
    public static void main (String args[]){
        facadeInstrumentos = new FacadeInstrumentos();
        listarInstrumentos();
    }
    public static void listarInstrumentos(){
    LinkedList<InstrumentosFacade> listaInstrumentosFacade = facadeInstrumentos.generarInstrumentosFacade();
    System.out.println("--------------------------------------------------------------------------------------------------");
    for(int x=0 ;x< listaInstrumentosFacade.size(); x++){
        System.out.println("name ----> "+listaInstrumentosFacade.get(x).getNameInstrumento()+ " / price $ >>> " + 
                listaInstrumentosFacade.get(x).getPriceInstrumento());
        InstrumentosFacade instrumento = listaInstrumentosFacade.get(x);
        System.out.println("description ---> " + instrumento.getDescriptionInstrumento()+" / category >>> " + 
                instrumento.getCategoryInstrumento());
    }
  }
}
