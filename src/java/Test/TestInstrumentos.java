
package Test;


import connector.ConexionObjetos;
import cruds.CRUDInstrumentos;
import java.sql.ResultSet;
import java.util.LinkedList;
import pojos.Instrumento;
/**
 *
 * @author FELIPE
 */
public class TestInstrumentos {
    static CRUDInstrumentos crudInstrumentos;
    public static void main (String args[]){
        /**
          try{
               ConexionObjetos co = ConexionObjetos.getInstance();
               ResultSet rs = co.getbConn().getSt().executeQuery("SELECT * FROM pelicula");
               while(rs.next()){
          System.out.println("name >>> " +rs.getString("name"));
        }
        rs.close();
          ConexionObjetos co2 = co.clone();
      } catch(Exception e){
          e.printStackTrace();
      }
    */
        
     crudInstrumentos = new CRUDInstrumentos();
       listarInstrumentos();
    
     crudInstrumentos = new CRUDInstrumentos();
     Instrumento instrumentoNuevo = new Instrumento();
     instrumentoNuevo.setName("guitarra electrica");
     instrumentoNuevo.setDescription("guitarra electrica, con aspecto similar al bajo electrico");
     instrumentoNuevo.setPrice(200000);
     instrumentoNuevo.setCategory(4); 
     crudInstrumentos.insertarInstrumento(instrumentoNuevo);
     listarInstrumentos();   
     System.out.println("NUEVO INSTRUMENTO");

  
     Instrumento instrumentoActualizado = new Instrumento();
     instrumentoActualizado.setId(18);
     instrumentoActualizado.setName("bateria");
     instrumentoActualizado.setDescription("conjunto de instrumentos de tipo percusion");
     instrumentoActualizado.setPrice(500000);
     instrumentoActualizado.setCategory(2);
     crudInstrumentos.modificarInstrumento(instrumentoActualizado);
     
     listarInstrumentos();
     System.out.println("INSTRUMENTO ACTUALIZADO");
   
     crudInstrumentos.borrarInstrumento(27);
     listarInstrumentos();
        System.out.println("INSTRUMENTO ELIMINADO");
   
    }
    public static void listarInstrumentos(){
    LinkedList<Instrumento> listaInstrumentos = crudInstrumentos.listarInstrumentos();
    System.out.println("--------------------------------------------------------------------------------------------------");
    for(int x=0 ;x< listaInstrumentos.size(); x++){
        System.out.println("name ----> "+listaInstrumentos.get(x).getName() + " / price $ >>> "+listaInstrumentos.get(x).getPrice());
        Instrumento instrumento = listaInstrumentos.get(x);
        System.out.println("description ---> " + instrumento.getDescription()+" / category >>> " +instrumento.getCategoryName());
    }
  }
}
