
package builderinstrumentos;

import cruds.CRUDInstrumentos;
import java.util.LinkedList;
import pojos.Instrumento;

/**
 *
 * @author FELIPE
 */
public class TestInstrumentoBuilder {
       private static cruds.CRUDInstrumentos crud = new CRUDInstrumentos();
       public static void main (String[] args){
               System.out.println("BUILDER --> INSTRUMENTOS CUERDAS >> ");
        LinkedList<Instrumento> listaInstrumentosCuerdas = crud.listarInstrumentosBuilder(1);
        for(int x = 0; x < listaInstrumentosCuerdas.size(); x++){
            System.out.println("----------------------------");
            System.out.println("nombre > " + listaInstrumentosCuerdas.get(x).getName() + " / precio > "
            + listaInstrumentosCuerdas.get(x).getPrice());
        }
        System.out.println("************************");
        System.out.println("BUILDER --> INSTRUMENTOS PERCUSION >>> ");
        LinkedList<Instrumento> listaInstrumentosPercusion = crud.listarInstrumentosBuilder(2);
        for(int y = 0; y < listaInstrumentosPercusion.size(); y++){
            System.out.println("----------------------------");
            System.out.println("nombre > " + listaInstrumentosPercusion.get(y).getName() + " / precio > "
            + listaInstrumentosPercusion.get(y).getPrice());
        }
        System.out.println("************************");
        System.out.println("BUILDER --> INSTRUMENTOS VIENTOS");
        LinkedList<Instrumento> listaInstrumentosVientos = crud.listarInstrumentosBuilder(3);
        for(int z =0; z < listaInstrumentosVientos.size(); z++){
            System.out.println("----------------------------");
            System.out.println("nombre > " + listaInstrumentosVientos.get(z).getName() + " / precio> "
            + listaInstrumentosVientos.get(z).getPrice());
        }
        System.out.println("************************");
        System.out.println("BUILDER --> INSTRUMENTOS ELECTRONICOS");
        LinkedList<Instrumento> listaInstrumentosElectronico = crud.listarInstrumentosBuilder(4);
        for(int z =0; z < listaInstrumentosElectronico.size(); z++){
            System.out.println("----------------------------");
            System.out.println("nombre > " + listaInstrumentosElectronico.get(z).getName() + " / precio> "
            + listaInstrumentosElectronico.get(z).getPrice());
        }
      
    }
}
