
package builderinstrumentos;

import pojos.Instrumento;

/**
 *
 * @author FELIPE
 */
public abstract class InstrumentoBuilder {
    protected Instrumento instrumentoB;
    
    public Instrumento getInstrumentoPatronBuilder(){
        return instrumentoB;
    }
    public void crearInstrumentoPatronBuilder(){
        instrumentoB = new Instrumento();
    }
    public abstract void construirInstrumento(int id, String name, String description, int price, int category );
}
