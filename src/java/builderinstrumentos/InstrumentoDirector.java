
package builderinstrumentos;

import pojos.Instrumento;

/**
 *
 * @author FELIPE
 */
public class InstrumentoDirector {
    private InstrumentoBuilder instrumentoBuilder;
    
    public void buildInstrumento(int id, String name, String description, int price, int category){
      instrumentoBuilder.crearInstrumentoPatronBuilder();
      instrumentoBuilder.construirInstrumento(id, name, description, price, category);
    }
    public void setInstrumentoBuilder(InstrumentoBuilder instrumentoB){
      instrumentoBuilder = instrumentoB;  
    }
    public Instrumento getInstrumentoPatronBuilder(){
        return instrumentoBuilder.getInstrumentoPatronBuilder();
    } 
}
