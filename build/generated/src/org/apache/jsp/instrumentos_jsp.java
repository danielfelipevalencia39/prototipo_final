package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.util.Iterator;
import pojos.Category;
import iteratorinstrumentos.IteratorListaCategorias;
import cruds.CRUDCategoriasInstrumentos;
import java.util.LinkedList;
import pojos.Instrumento;
import cruds.CRUDInstrumentos;

public final class instrumentos_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("<link rel=\"stylesheet\" href=\"https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css\" integrity=\"sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2\" crossorigin=\"anonymous\">\n");
      out.write("<script src=\"https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js\" integrity=\"sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx\" crossorigin=\"anonymous\"></script>\n");
      out.write("<body style=\"background-image: url('fondo1.jpg'); background-size: 100%\">\n");
      out.write("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <title>\n");
      out.write("            INSTRUMENTOS\n");
      out.write("        </title>\n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("        <hr>\n");
      out.write("        <br>\n");
      out.write("        <center> <h1> Instrumentos Disponibles </h1>\n");
      out.write("        <table border =\"1\" class=\"table table-striped table bordered\"  cellpadding=\"0\" cellspacing=\"0\" width=\"75%\" bgcolor=\"white\">\n");
      out.write("            <tr class=\"thread-dark\">\n");
      out.write("                <td>Identificativo</td>\n");
      out.write("                <td>Nombre Instrumento</td>\n");
      out.write("                <td>Descripcion</td>\n");
      out.write("                <td>Precio</td>\n");
      out.write("                <td>Categoria</td>\n");
      out.write("            </tr>\n");
      out.write("            ");
  
            CRUDInstrumentos crudInstrumentos = new CRUDInstrumentos();
            LinkedList<Instrumento> lista = crudInstrumentos.listarInstrumentos();
            for(int i =0; i< lista.size();i++){
                Instrumento instrumento = lista.get(i);
                out.println("<tr>");
                out.println("<td>"+ instrumento.getId() +"</td>");
                out.println("<td>"+ instrumento.getName() +"</td>");
                out.println("<td>"+ instrumento.getDescription() +"</td>");
                out.println("<td>"+ instrumento.getPrice() +"</td>");
                out.println("<td>"+ instrumento.getCategory() +"</td>");
                out.println("</tr>");
            }
            
      out.write("\n");
      out.write("        </table>\n");
      out.write("        <br>\n");
      out.write("        <a href=\"listaCategoriasInstrumentos.jsp\">Lista por Categorias de Instrumentos</a>\n");
      out.write("        <hr>\n");
      out.write("        <br>\n");
      out.write("        <form name =\"formularioinstrumentos\" action=\"InstrumentsControler\" method=\"get\">\n");
      out.write("            Codigo ><input type=\"text\" name=\"id\"><br>\n");
      out.write("            Nombre Instrumento > <input type=\"text\" name=\"name\"><br>\n");
      out.write("            Descripcion ><input type=\"text\" name=\"description\"><br>\n");
      out.write("            Precio ><input type=\"text\" name=\"price\" value=\"0\"><br>\n");
      out.write("            Patron Iterator >>> <br>\n");
      out.write("            Categoria >\n");
      out.write("            <select name=\"category\">\n");
      out.write("                ");

                    CRUDCategoriasInstrumentos crudCategorias = new CRUDCategoriasInstrumentos();
                    IteratorListaCategorias listaCategorias = new IteratorListaCategorias();
                    LinkedList<Category> listaCat = crudCategorias.listarCategorias();
                    
                    for(int x=0 ; x<listaCat.size();x++){
                        listaCategorias.add(listaCat.get(x).getId());
                    }
                    Iterator<Category> iteratorCategorias = listaCategorias.iterator();
                    while(iteratorCategorias.hasNext()){
                        Category categoria = (Category)iteratorCategorias.next();
                        out.println("<option value =\"" + categoria.getId() + "\">" +
                                categoria.getName() + "</option>");
                    }
                
      out.write("\n");
      out.write("            </select>\n");
      out.write("            <hr>\n");
      out.write("            <br>\n");
      out.write("            <input type=\"submit\" name=\"insertarinstrumento\"value=\"Guardar Instrumento\" class=\"btn btn-success\" >\n");
      out.write("            <hr>\n");
      out.write("            <br>\n");
      out.write("            <input type=\"submit\" name=\"editarinstrumento\" value=\"Actualizar Instrumento\" class=\"btn btn-primary\">\n");
      out.write("            <hr>\n");
      out.write("            <br>\n");
      out.write("            <input type=\"submit\" name=\"eliminarinstrumento\" value=\"Borrar Instrumento\" class=\"btn btn-danger\">\n");
      out.write("            <hr>\n");
      out.write("            <br>\n");
      out.write("        </form>\n");
      out.write("    </body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
