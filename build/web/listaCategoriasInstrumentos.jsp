<%-- 
    Document   : listaCategoriasInstrumentos
    Created on : 25/11/2020, 01:02:58 PM
    Author     : FELIPE
--%>
<%@page import="pojos.Category"%>
<%@page import="cruds.CRUDCategoriasInstrumentos"%>
<%@page import="java.util.LinkedList"%>
<%@page import="pojos.Instrumento"%>
<%@page import="cruds.CRUDInstrumentos"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
<body style="background-image: url('fondo.jpg'); background-size: 100%">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Lista Categorias</title>
    </head>
    <body>

       <center><h1>Lista Categorias Instrumentos</h1>
        <br>
        <table border ="1" class="table table-striped table bordered" cellpadding="0" cellspacing="0" width="20%" bgcolor="white">
            <tr>
                <td> <center>LISTA INSTRUMENTOS CATEGORIA CUERDAS >>> PATRON BUILDER </center>
                    <table border="1" class="table table-striped table bordered" cellpadding="0" cellspacing="0" width="75%" bgcolor="white">
                        <tr class="thread-dark">
                            <td>Codigo Instrumento</td>
                            <td>Nombre</td>
                            <td>Descripcion</td>
                            <td>Precio</td>
                        </tr>
                        <%
                        CRUDInstrumentos crudInstrumentos = new CRUDInstrumentos();
                        LinkedList<Instrumento> listaInstrumentosCuerdas = crudInstrumentos.listarInstrumentosBuilder(1);
                        for(int i=0;i<listaInstrumentosCuerdas.size();i++){
                            out.println("<tr>");
                            out.println("<td>"+listaInstrumentosCuerdas.get(i).getId()+"</td>");
                            out.println("<td>"+listaInstrumentosCuerdas.get(i).getName() +"</td>");
                            out.println("<td>"+listaInstrumentosCuerdas.get(i).getDescription()+"</td>");
                            out.println("<td>"+listaInstrumentosCuerdas.get(i).getPrice()+"</td>");
                            out.println("</tr>");
                        }
                        %>
                    </table>
                </td>
               </tr>
               <tr>
               <td> <center>LISTA INSTRUMENTOS POR CATEGORIA PERCUSION >>> PATRON BUILDER</center>
                   <table border="1" class="table table-striped table bordered" cellpadding="0" cellspacing="0" width="75%" bgcolor="white">
                       <tr class="thread-dark">
                           <td>Codigo Instrumento</td>
                           <td>Nombre</td>
                           <td>Descripcion</td>
                           <td>Precio</td>
                       </tr>
                       <%
                       LinkedList<Instrumento> listaInstrumentosPercusion = crudInstrumentos.listarInstrumentosBuilder(2);
                       for(int x=0;x<listaInstrumentosPercusion.size();x++){
                           out.println("<tr>");
                           out.println("<td>"+listaInstrumentosPercusion.get(x).getId()+"</td>");
                           out.println("<td>"+listaInstrumentosPercusion.get(x).getName()+"</td>");
                           out.println("<td>"+listaInstrumentosPercusion.get(x).getDescription()+"</td>");
                           out.println("<td>"+listaInstrumentosPercusion.get(x).getPrice()+"</td>");
                       }
                       %>
                   </table>
               </td>
               </tr>
               <tr>
                   <td><center>LISTA INSTRUMENTOS CATEGORIA VIENTOS >>> PATRON BUILDER</center>
                       <table border="1" class="table table-striped table bordered" cellpadding="0" cellspacing="0" width="75%" bgcolor="white">
                           <tr class="thread-dark">
                           <td>Codigo Instrumento</td>
                           <td>Nombre</td>
                           <td>Descripcion</td>
                           <td>Precio</td>
                           </tr>
                           <%
                           LinkedList<Instrumento> listaInstrumentosVientos = crudInstrumentos.listarInstrumentosBuilder(3);
                           for(int y=0; y<listaInstrumentosVientos.size();y++){
                           out.println("<tr>");
                           out.println("<td>"+listaInstrumentosVientos.get(y).getId()+"</td>");
                           out.println("<td>"+listaInstrumentosVientos.get(y).getName()+"</td>");
                           out.println("<td>"+listaInstrumentosVientos.get(y).getDescription()+"</td>");
                           out.println("<td>"+listaInstrumentosVientos.get(y).getPrice()+"</td>");
                           }
                           
                           %>
                       </table>
                    </td>
                  </tr>
                   <tr>
                       <td><center>LISTA INSTRUMENTOS CATEGORIA ELECTRONICOS >>> PATRON BUILDER</center>
                       <table border="1" class="table table-striped table bordered" cellpadding="0" cellspacing="0" width="75%" bgcolor="white">
                           <tr class="thread-dark">
                           <td>Codigo Instrumento</td>
                           <td>Nombre</td>
                           <td>Descripcion</td>
                           <td>Precio</td>
                           </tr>
                           <%
                           LinkedList<Instrumento> listaInstrumentosElectronicos = crudInstrumentos.listarInstrumentosBuilder(4);
                           for(int z=0; z<listaInstrumentosElectronicos.size();z++){
                           out.println("<tr>");
                           out.println("<td>"+listaInstrumentosElectronicos.get(z).getId()+"</td>");
                           out.println("<td>"+listaInstrumentosElectronicos.get(z).getName()+"</td>");
                           out.println("<td>"+listaInstrumentosElectronicos.get(z).getDescription()+"</td>");
                           out.println("<td>"+listaInstrumentosElectronicos.get(z).getPrice()+"</td>");
                           }
                           
                           %>
                       </table>
                </td>
               </tr>
        </table>
               <br>
               <br>
                <hr>
                <table border ="1" class="table table-striped table bordered" cellpadding="0" cellspacing="0" width="20%" bgcolor="white">
                    <td><center>TODOS LOS INSTRUMENTOS >>> PATRON FACADE</center>
                 <table border="1" class="table table-striped table bordered" cellpadding="0" cellspacing="0" width="75%" bgcolor="white">
                           <tr class="thread-dark">
                           <td>Codigo Instrumento</td>
                           <td>Nombre</td>
                           <td>Descripcion</td>
                           <td>Precio</td>                      
                           <td>Nombre Categoria</td>
                           </tr>
                           <%
                           CRUDCategoriasInstrumentos crudCategorias = new CRUDCategoriasInstrumentos();
                           LinkedList<Instrumento> listaInstrumentos = crudInstrumentos.listarInstrumentos();
                           LinkedList<Category> listaCategoriasInstrumento = crudCategorias.listarCategorias();
                           for(int q=0; q<listaInstrumentos.size();q++){
                               for(int p=0; p< listaCategoriasInstrumento.size(); p++){
                                   if(listaInstrumentos.get(q).getCategory() == listaCategoriasInstrumento.get(p).getId()){
                                       listaInstrumentos.get(q).setCategoryName(listaCategoriasInstrumento.get(p).getName());
                                       Instrumento instrumento = listaInstrumentos.get(q);
                                       Category categoria = listaCategoriasInstrumento.get(p);
                                       out.println("<tr>");
                                       out.println("<td>"+instrumento.getId()+"</td>");
                                       out.println("<td>"+instrumento.getName()+"</td>");
                                       out.println("<td>"+instrumento.getDescription()+"</td>");
                                       out.println("<td>"+instrumento.getPrice()+"</td>");
                                       //out.println("<td>"+instrumento.getCategory()+"</td>"); 
                                      // out.println("<td>"+categoria.getId()+"</td>"); 
                                       out.println("<td>"+categoria.getName()+"</td>"); 
                                   }
                                }
                           }
                           %>
                        </table>
                      </table>
                     </td>
                  </tr>    
          <br>
          <hr>
        <a href="instrumentos.jsp">Regresar a Inicio</a>
        <hr>
    </body>
</html>
