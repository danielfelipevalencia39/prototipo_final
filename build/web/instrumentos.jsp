<%-- 
    Document   : instrumentos
    Created on : 5/11/2020, 12:09:25 PM
    Author     : FELIPE
--%>
<%@page import="java.util.Iterator"%>
<%@page import="pojos.Category"%>
<%@page import="iteratorinstrumentos.IteratorListaCategorias"%>
<%@page import="cruds.CRUDCategoriasInstrumentos"%>
<%@page import ="java.util.LinkedList"%>
<%@page import ="pojos.Instrumento"%>
<%@page import ="cruds.CRUDInstrumentos"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
<body style="background-image: url('fondo1.jpg'); background-size: 100%">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>
            INSTRUMENTOS
        </title>
    </head>
    <body>
        <hr>
        <br>
        <center> <h1> Instrumentos Disponibles </h1>
        <table border ="1" class="table table-striped table bordered"  cellpadding="0" cellspacing="0" width="75%" bgcolor="white">
            <tr class="thread-dark">
                <td>Identificativo</td>
                <td>Nombre Instrumento</td>
                <td>Descripcion</td>
                <td>Precio</td>
                <td>Categoria</td>
            </tr>
            <%  
            CRUDInstrumentos crudInstrumentos = new CRUDInstrumentos();
            LinkedList<Instrumento> lista = crudInstrumentos.listarInstrumentos();
            for(int i =0; i< lista.size();i++){
                Instrumento instrumento = lista.get(i);
                out.println("<tr>");
                out.println("<td>"+ instrumento.getId() +"</td>");
                out.println("<td>"+ instrumento.getName() +"</td>");
                out.println("<td>"+ instrumento.getDescription() +"</td>");
                out.println("<td>"+ instrumento.getPrice() +"</td>");
                out.println("<td>"+ instrumento.getCategory() +"</td>");
                out.println("</tr>");
            }
            %>
        </table>
        <br>
        <a href="listaCategoriasInstrumentos.jsp">Lista por Categorias de Instrumentos</a>
        <hr>
        <br>
        <form name ="formularioinstrumentos" action="InstrumentsControler" method="get">
            Codigo ><input type="text" name="id"><br>
            Nombre Instrumento > <input type="text" name="name"><br>
            Descripcion ><input type="text" name="description"><br>
            Precio ><input type="text" name="price" value="0"><br>
            Patron Iterator >>> <br>
            Categoria >
            <select name="category">
                <%
                    CRUDCategoriasInstrumentos crudCategorias = new CRUDCategoriasInstrumentos();
                    IteratorListaCategorias listaCategorias = new IteratorListaCategorias();
                    LinkedList<Category> listaCat = crudCategorias.listarCategorias();
                    
                    for(int x=0 ; x<listaCat.size();x++){
                        listaCategorias.add(listaCat.get(x).getId());
                    }
                    Iterator<Category> iteratorCategorias = listaCategorias.iterator();
                    while(iteratorCategorias.hasNext()){
                        Category categoria = (Category)iteratorCategorias.next();
                        out.println("<option value =\"" + categoria.getId() + "\">" +
                                categoria.getName() + "</option>");
                    }
                %>
            </select>
            <hr>
            <br>
            <input type="submit" name="insertarinstrumento"value="Guardar Instrumento" class="btn btn-success" >
            <hr>
            <br>
            <input type="submit" name="editarinstrumento" value="Actualizar Instrumento" class="btn btn-primary">
            <hr>
            <br>
            <input type="submit" name="eliminarinstrumento" value="Borrar Instrumento" class="btn btn-danger">
            <hr>
            <br>
        </form>
    </body>
</html>
